@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-8 offset-2 pb-5">
            <h4>Feeds</h4>
        </div>
    @foreach($posts as $post)
    <div class="row justify-content-center">
        <div class="col-8 offset-2">
            <a href="/dashboard/{{ $post->user->id }}">
                <img src="/storage/{{$post->image}} ?? 'N/A'" class="w-100" style="max-width: 300px;">
            </a>
        </div>
        <div class="row">
            
        </div>

        <div class="col-8 offset-2 pt-2 pb-5">
            <div>
                <p>
                    <img src="{{$post->user->profile->profileImage()}} ?? 'N/A'" class="rounded-circle w-100" style="max-width: 30px;">
                    <span class="font-weight-bold">
                        <a href="/dashboard/{{$post->user->id}}">
                            <span class="text-dark">{{$post->user->username}} --></span>
                        </a>
                    </span> {{$post->caption}}</p>

                <p>{{$post->content}}</p>
            </div>
        </div>

    </div>
    @endforeach
    <div class="row col-12 d-flex justify-content-center">
        {{$posts->links()}}
    </div>
</div>
@endsection
