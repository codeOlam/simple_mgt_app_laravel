@extends('layouts.app')

@section('content')
<div class="container">
    Connect With Researchers,<br>
    Follow them to find out what is happening in you interest ecosystem.
    <div class="row col-8 offset-2 justify-content-center">
        @foreach($get_all_profiles as $reseacher)
        <div class="pt-5 pr-5 ">
            <a href="/dashboard/{{$reseacher->user_id}}">
                <img src="/storage/{{$reseacher->image}} ?? 'N/A'" class="rounded-circle w-100" style="max-width: 50px;">
            </a>
            <p class="font-weight-bold align-text-center">@_{{$reseacher->title}}</p>
        </div>

        @endforeach
    </div>
</div>
@endsection
