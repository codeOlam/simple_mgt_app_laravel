@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/post" enctype="multipart/form-data" method="post">
        @csrf
        <div class="row">
            <div class="col-8 offset-2">
                <div class="row">
                    <h3>Share your latest Accademic Findings...!</h3>
                </div>
                <div class="form-group row">
                    <label for="caption" class="col-md-4 col-form-label">Feed Caption</label>
                        <input id="caption" 
                        type="caption" 
                        class="form-control @error('caption') is-invalid @enderror" 
                        name="caption" 
                        value="{{ old('caption') }}" autocomplete="caption">

                        @error('caption')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div> 
                <!-- end-row for Post caption -->


                <div class="row">
                    <label for="image" class="col-md-4 col-form-label">Upload Photo</label>
                    <input type="file", class="form-control-file" id="image" name="image">

                        @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror            
                </div> 
                <!-- end-row for Upload Image --> 

                <div class="form-group row pt-3">
                    <label for="content" class="col-md-4 col-form-label">Feed content</label>
                        <textarea class="form-control @error('content') is-invalid @enderror" 
                        id="content"
                        type="content" 
                        name="content"
                        value="{{ old('content') }}" autocomplete="content">
                        </textarea>

                        @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div> 
                <!-- end-row for content --> 

                <div class="row pt-4">
                    <button class="btn btn-primary"> Share Post</button>
                </div>
                <!-- end-button row -->
            </div>
        </div>   
    </form>
</div>
@endsection
