@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-3 pt-5 pr-5">
                <img src="{{$user->profile->profileImage() ?? 'N/A'}}" class="rounded-circle w-100">
            </div>
            <div class="col-9 pt-5 pl-5">
                <div class="d-flex justify-content-between align-items-baseline">
                    <div class="d-flex align-items-center pb-3">
                        <h4>{{$user->username}}</h4>

                        <follow-botton user-id="{{ $user->id }}" follows="{{$follows}}"></follow-botton>
                    </div>

                    @can('update', $user->profile)
                        <a href="/post/create">Add New Feed</a> 
                    @endcan
                </div>

                @can('update', $user->profile)
                    <a href="/dashboard/{{$user->id}}/edit">Edit Profile</a>
                @endcan

                <div class="d-flex">
                    <div class="pr-5"><strong>{{ $feedsCount }}</strong> feeds</div>
                    <div class="pr-5"><strong>{{ $followersCount }}</strong> followers</div>
                    <div class="pr-5"><strong>{{ $followingCount }}</strong> following</div>
                </div>
            <div class="pt-4 font-weight-bold">
                <h5><strong>{{$user->name}}</strong></h5>
            </div>
            <div class="pt-4 font-weight-bold">{{$user->profile->title}}</div>
                <div>{{$user->profile->description}}</div>
                <div>{{$user->profile->url ?? 'N/A'}}</div>
            
            </div> 
        </div>
    </div>

    <div class="row col-10 offsett-2 pt-4 d-flex">
        @foreach($user->posts as $post)
            <div class="col-4 pb-5">
                <a href="/post/{{$post->id}}">
                    <h4 class="text-center pb-4"><strong>{{$post->caption}}</strong></h4>
                </a>

                <a href="/post/{{$post->id}}">
                    <img src="/storage/{{$post->image}} ?? 'N/A'" class="w-100">
                </a>
            </div>
        @endforeach
    </div>

</div>
@endsection
