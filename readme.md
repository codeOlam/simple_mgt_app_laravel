![Laravel Logo](public/img/laravel-pg.png?raw=true "banner")


## App Url
The application is hosted on:
https://research-network.herokuapp.com/

Although Images/files are not handled well on heroku so you may not be seeing uploaded images while surfing the application but you can add new feeds with out images to see what it looks like.


## About
Laravel is a web-framework written based on php. This app is built ontop of Laravel Framework 6.16.0 . Just a basic intoduction to laravel's cool features. This application is a light researcher's accademic network. where each interested researcher could create an account and build their profile, adding pictures, posts and so on, and share links with friends. 


This app features the following:

-- CRUD in Laravel:

Here you CREATE (add Register user and create post), READ (view other users post that you are following), UPDATE(update your profile)



## Technology and Requirements
1. Laravel Framework 6.16.0
2. php 7.3.7
3. postgresql(using pyscopg2 as the adapter for python3)
4. Boostrap 4.3.x (for front end)
5. Vue -for auth
6. npm 6.4.1
7. node 8.12.0

## Installations
1. [laravel documentation](https://laravel.com/docs)
2. [Composer documentation](https://getcomposer.org/doc/)



## Recommendations
There is more you could still do with his app, you could create functionalities to 
1. delete user account, 
2. default display feed for newly registered users
3. more UI
4. search function

you can fork the code and use it



## Resources
1. [Laravel Doc](https://laravel.com/docs)
2. [Composer documentation](https://getcomposer.org/doc/)


### Other Resources
1. [Flame code logo](https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.123rf.com%2Fphoto_101009696_stock-vector-flame-code-logo-icon-design.html&psig=AOvVaw3EcrQZzV8bIy5MsHzSCbQc&ust=1582473101532000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLj8qpbC5ecCFQAAAAAdAAAAABAD)
2. [laravel_pg image](https://www.google.com/imgres?imgurl=https%3A%2F%2Fi.morioh.com%2Fc9fa4e5578.png&imgrefurl=https%3A%2F%2Fmorioh.com%2Fp%2Ffc4334d88466&tbnid=8Bfiftbzpz9ipM&vet=12ahUKEwj73dq8wuXnAhUR_hoKHctiCvQQMygIegUIARDaAQ..i&docid=2YflA4uo1rpglM&w=1280&h=720&q=laravel%20%2B%20postgresql&client=firefox-b&ved=2ahUKEwj73dq8wuXnAhUR_hoKHctiCvQQMygIegUIARDaAQ)
3. [infinity binary code](https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.123rf.com%2Fphoto_113143365_stock-vector-abstract-sign-infinity-from-binary-code-vector.html&psig=AOvVaw0gKlTYU7eOE6Rn37EXqdsg&ust=1582475949369000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKDJj83M5ecCFQAAAAAdAAAAABAF)
