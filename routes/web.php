<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Mail\ConfirmationMail;



Auth::routes();

Route::get('/email', function(){
	return new ConfirmationMail();
});

// Route::get('/home', 'DashBoardController@index')->name('home');
Route::post('follow/{user}', 'FollowController@store');

Route::get('/', 'PostsController@index');
Route::get('/researchers', 'PostsController@getAllResearchers');
Route::get('/post/create', 'PostsController@create');
Route::post('/post', 'PostsController@store');
Route::get('/post/{post}', 'PostsController@show');
Route::get('/dashboard/{user}', 'DashBoardController@index')->name('dashboard.show');
Route::get('/dashboard/{user}/edit', 'DashBoardController@edit')->name('dashboard.edit');
Route::patch('/dashboard/{user}', 'DashBoardController@update')->name('dashboard.update');

