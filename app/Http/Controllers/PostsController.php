<?php

namespace App\Http\Controllers;

use App\Post;
Use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}


    public function index(){
        $users = auth()->user()->following()->pluck('profiles.user_id'); #this is will get all id of users being followed

        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(5); #grab latest posts from all followed users



        return view('posts.index', compact('posts',));
    }

    public function getAllResearchers(){
        $get_all_profiles = Profile::all();  #to get all registered users on the researchNetwork app
        #dd($get_all_profiles);

        return view('posts.researchers', compact('get_all_profiles'));
    }

    public function create(){
    	return view('posts.create');
    }

    public function store(){

    	$data = request()->validate([
    	    		'caption' => 'required',
    	    		'image' => '',
    	    		'content'=>'required',
    	    	]);
    	// dd(request()->all()); //dd(die and dump)

    	// $post= new \App\Post();

    	// // $post->caption=$data['caption'];
    	// // $post->save();

        if (request('image')){
            $imagePath = request('image')->store('uploads', 'public');

            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1200);
            $image->save();  

            $imageArray = ['image'=>$imagePath];
        }



    	//auth()->user()->posts()->create($data);
    	auth()->user()->posts()->create(array_merge($data,
            $imageArray ?? []));

    	// \APP\Post::create($data);
    	return redirect('/dashboard/' . auth()->user()->id);
    }

    public function show(\App\Post $post){
        return view('posts.show_post', ['post'=>$post]);
    } 
}
